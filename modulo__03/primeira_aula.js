// console.log("cheguei");

var teste = "123";
let teste1 = "1233";
const teste2 = 1222;

var teste = "111";
// teste1 = "222";
// teste2 = "aqui";
// console.log(teste2);

{
    let teste1 = "aqui mudou";
}

const pessoa = {
    nome: "Juliana",
    idade: 21,
    endereco: {
        logradouro: "rua Gaston",
        numero: 147
    }
}

Object.freeze(pessoa);

pessoa.nome = "Juliana Costa"

// console.log(pessoa);

function somar(valor1, valor2) {
    // console.log(valor1 + valor2);
}
// sinal de + concatena em caso de string + inteiro
 somar(2, 3);

 function ondeMoro(cidade){
    //  console.log("Eu moro em " + cidade + "E sou muito feliz");
    //  console.log(`Eu moro em  ${cidade} e sou muito feliz`);
     console.log(`Eu moro em  ${cidade} e sou muito feliz`);
}

// ondeMoro("Poa");

function fruteira(){
    let texto = "Banana"
                +"\n"
                "Ameixa"
                +"\n"
                "Goiaba"
                +"\n"
                "Pessego"
                +"\n";
    let newTexto = `Banana
                    Ameixa
                    Goiaba
                    Pessego`;
    //  console.log(newTexto);

}

fruteira();

function quemSou(pessoa){
    // console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade}`);
}

quemSou(pessoa);

let funcaoSomarVar = function(a,b, c=0){
    return a + b + c;
}

let add = funcaoSomarVar
let resultado = add(3, 2)
// console.log(resultado);


const {nome:n, idade} = pessoa
const {endereco: { logradouro, numero}} = pessoa
// console.log(n,idade);
// console.log(logradouro, numero);

const array = [1, 3, 4, 8];
const [ n1, , n2, , n3 = 9] = array
console.log(n1, n2, n3);

function testarPessoa({nome, idade}){
    console.log(nome,idade);
}

testarPessoa(pessoa);

let a1 = 42;
let b1 = 15;
console.log(a1, b1);

[a1, b1] = [b1, a1];

console.log(a1, b1);
