class Pokemon{
    constructor( obj ){
        this.nome = obj.name
        this.id = obj.id
        this.altura = obj.height
        this.peso = obj.weight
        this.tipo = obj.types
        this.estatistica = obj.stats
        this.foto = obj.sprites

    }
}