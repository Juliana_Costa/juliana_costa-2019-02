let pokeApi = new PokeApi();
// let pokemonEspecifico = pokeApi.buscarEspecifico(12);
// pokemonEspecifico.then(pokemon => {
//     let poke = new Pokemon( pokemon );
//     rederizacaoPokemon( poke );
// })

function rederizacaoPokemon( pokemon ){
    let dadosPokemon = document.getElementById('dadosPokemon');
    let nome = dadosPokemon.querySelector('.nome');
    nome.innerHTML = pokemon.nome;

    let id = dadosPokemon.querySelector('.id');
    id.innerHTML = pokemon.id;

    let altura = dadosPokemon.querySelector('.altura');
    altura.innerHTML = `${pokemon.altura}0 cm`; 

    let peso = dadosPokemon.querySelector('.peso');
    pokemon.peso = pokemon.peso / 10; //retorna em kg
    peso.innerHTML = `${pokemon.peso}Kg`;
    
    let tipo = dadosPokemon.querySelector('.tipo');
    let lista = [];
    for( let i = 0; i < pokemon.tipo.length; i++){
        lista.push(pokemon.tipo[i].type.name);
    }
    tipo.innerHTML = `<li> ${lista.join('</li><li>')}</li>`;
    let estatistica = dadosPokemon.querySelector('.estatistica');
    let listaEstatistica = [];
    for( let i = 0; i < pokemon.estatistica.length; i++){
        listaEstatistica.push(`${pokemon.estatistica[i].stat.name}:${pokemon.estatistica[i].base_stat}%`); 
    }
    estatistica.innerHTML = `<li> ${listaEstatistica.join('</li><li>')}</li>`;

    let foto = document.getElementById('foto'); //pegar as fotos dos pokemons atraves do Id
    foto.src = pokemon.foto.front_default;
    
}

let campo = document.getElementById('idDigitado');
function verificaCampo(idDigitado){
    if ((idDigitado == "") || (idDigitado <= 0) || (idDigitado > 802)){
        return false;
    }else{
        return true;
    }
} 

function chamandoBuscarEspecifico(id){
    let resultado = pokeApi.buscarEspecifico(id);
    resultado.then(pokemon => {
    let poke = new Pokemon( pokemon );
    rederizacaoPokemon( poke );
})
}

campo.addEventListener('blur', function(){ // blur -> evento
    let idDigitado = campo.value;
    let verifica = verificaCampo(idDigitado);
    if(!verifica){
        alert("Digite um ID válido");//aceitar somente os ID validos
    }else{
        chamandoBuscarEspecifico(idDigitado);
    
    }
})

function testeBotao(){
    let funcao;
    funcao = document.getElementById("botao").OnClick;
    let numero = Math.floor(Math.random() * 802); // funcao relacionada ao 'estou com sorte' para exibir pokemons aleatorios
    console.log(numero);
    chamandoBuscarEspecifico(numero);
 }

campo.addEventListener('blur', function(){ // blur -> evento
    let idDigitado = campo.value;
    let verifica = verificaCampo(idDigitado);
    if(!verifica){
        alert("Digite um ID válido");//aceitar somente os ID validos
    }else{
        let resultado = pokeApi.buscarEspecifico(idDigitado);
        resultado.then(pokemon => {
        let poke = new Pokemon( pokemon );
        rederizacaoPokemon( poke );
    })
    }
})




































