const pi = 3.14;

function calcularCirculo({tipoCalculo:tipo, raio}){
    return Math.ceil(tipo =="A"?Math.PI=Math.pow(raio,2):2*Math.PI*raio) ;
}
let circulo= {
    raio:3,
    tipoCalculo:"A"
}

console.log(calcularCirculo(circulo));

calcularCirculo("a",5);

/*function naoBissexto(ano){
    return (ano%400 === 0)||(ano%4==0 && ano%100!==0)?false:true;
}*/


let naoBissexto=ano=> (ano%400 === 0)||(ano%4==0 && ano%100!==0)?false:true;
     /*const testes={
         diaAula:"Segundo",
         local:"DBC",
         naoBissexto(ano){
             return (ano%400 === 0)||(ano%4==0 && ano%100!==0)?false:true;
         }
     }*/


console.log(testes.naoBissexto(2016));

//naoBissexto(2020);

function somarPares(numeros){
    let resultado=0;
    for(let i=0;i<array.length;i++){
        if(i%2==0){
            resultado=numeros[i];
        }
    }
    return resultado;
}

//console.log(somarPares([1,56,4.34,6,-2]));

//somarPares(array);

/*function adicionar(n1){
    return function(n2){
        return n1 + n2;
    }
}*/
//let soma = adicionar(2)(4);

//console.log(soma);

//let adicionar= n1=>n2=>n1+n2;
//console.log(adicinar(3)(4));//7
//console.log(adicinar(5642)(8749));//14391

/*const is_divisivel=(divisor,numero)=>!(numero % divisor);
const divisor=2;
console.log(is_divisivel(divisor,20));
console.log(is_divisivel(divisor,11));
console.log(is_divisivel(divisor,12));*/

const divisivelPor=divisor=>numero=>!(numero %divisor);
const is_divisivel=divisivelPor(2);
const is_divisivel3=divisivelPor(3);
//console.log(is_divisivel(20));
//console.log(is_divisivel(11));
//console.log(is_divisivel(12));

function arredondar(numero,precisao=2){
    const fator=Math.pow(10,precisao)
    return Math.ceil(numero*fator)/fator;
}

let moedas=(function(){
    function imprimirMoeda(params){
        function arredondar(numero,precisao=2){
            const fator=Math.pow(10,precisao)
            return Math.ceil(numero*fator)/fator;
        }
    const{
        numero,
        separadorDecimal,
        separadorMilhar,
        colocarMoeda,
        colocarNegativo
    }=params
}

    let qtdCasasMilhares=3;
    let stringBuffer=[]
    let parteDecimal= arredondar(Math.abs(numero)%1)
    let parteInteira=Math.trunc(numero)
    let parteInteiraString=Math.abs(parteInteira).toString();
    let parteInteiraTamanho= parteInteiraString.length

    let c=1;
    while(parteInteiraString.length>0){
        if(c %qtdCasasMilhares==0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho-c)}`)
            parteInteiraString= parteInteiraString.slice(0,parteInteiraTamanho-c)
        }else if(parteInteiraString.length<qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString= ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)
    
    let decimalString=parteDecimal.toString().replace('0','').padStart(2,'0')
    const numeroFormatado=`${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
    return parteInteira>=0? colocarMoeda(numeroFormatado):colocarNegativo(colocarMoeda(numero))
}

    function imprimirBRL(numero){
    return  imprimirMoeda(
    {
        numero,
        separadorMilhar:'.',
        separadorDecimal:',',
        colocarMoeda: numeroFormatado=> `RS ${numeroFormatado}`,
        colocarNegativo: numeroFormatado=>`-${numeroFormatado}`,       
    }
    )
}

function imprimirGBP(numero){
    return  imprimirMoeda(
    {
        numero,
        separadorMilhar:',',
        separadorDecimal:'.',
        colocarMoeda: numeroFormatado=> `£ ${numeroFormatado}`,
        colocarNegativo: numeroFormatado=>`-£ ${numeroFormatado}`,         
    }
    )
}

function imprimirFR(numero){
    return  imprimirMoeda(
    {
        numero,
        separadorMilhar:'.',
        separadorDecimal:',',
        colocarMoeda: numeroFormatado=> `€ ${numeroFormatado}`,
        colocarNegativo: numeroFormatado=>`€-${numeroFormatado}`,         
    }
    )
}

console.log(moedas.imprimirBRL(0));
console.log(moedas.imprimirBRL(3498.99));
console.log(moedas.imprimirBRL(-3498.99));
console.log(moedas.imprimirBRL(23123.49));

console.log(moedas.imprimirGBP(0));
console.log(moedas.imprimirGBP(3498.99));
console.log(moedas.imprimirGBP(-3498.99));
console.log(moedas.imprimirGBP(23123.49));

console.log(moedas.imprimirFR(0));
console.log(moedas.imprimirFR(3498.99));
console.log(moedas.imprimirFR(-3498.99));
console.log(moedas.imprimirFR(23123.49));