function cardapioIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
    // i = cardapio.length
  
    if ( comLactose ) {
      cardapio.push('pastel de queijo')
    }
    cardapio = [...cardapio,'pastel de carne','empada de legumes marabijosa' ]
    // cardapio = cardapio.concat( [
    //   'pastel de carne',
    //   'empada de legumes marabijosa'
    // ] )
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
  
      cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1)
      cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }
  
    let resultado = cardapio
                          //.filter(alimento => alimento == 'cuca de uva')
                          .map(alimento => alimento.toUpperCase());
    return resultado;
  
  }
  console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
  
  function criarSanduiche(pao, recheio, queijo) {
      console.log(`Seu sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)
  }
  
  const ingredientes = ['3 queijos', 'Frango', 'cheddar']
  criarSanduiche(... ingredientes);
  
  function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
  }
  receberValoresIndefinidos([1, 3, 4, 5]);
  
  console.log(... "Juliana");

//   let inputTeste = document.getElementById('campoTeste')
//   console.log(inputTeste);
//   inputTeste.addEventListener('blur', function(){
//       console.log("chegou aqui");
//   })

 
function multiplicar(valores){
    let primeiroValor = valores[0];
    let resultados = [];
    for (let i = 1; i < valores.length; i++){
        resultados.push(primeiroValor*valores[i]);
    }
    return resultados;
}

let resule = multiplicar([5,3,4]);
let result = multiplicar([5,3,4,5]);
console.log(resule);
console.log(result);

// let inputTeste = document.getElementById('campoTeste')
// inputTeste 

// let objetoTeste = {
//   nome: "Evelin"
// }

String.prototype.correr = function(upper = false){
  let texto = `${this} estou Correndo`;
  return upper ? texto.toUpperCase() : texto;
}

console.log("eu".correr());