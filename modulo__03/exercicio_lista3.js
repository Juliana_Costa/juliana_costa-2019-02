let moedas = ( function() {
    
  //Tudo que é privado aqui
  function imprimirMoeda( params ){
      
      function arredondar(numero, precisao = 2) {
          const fator = Math.pow( 10, precisao )
          return Math.ceil( numero * fator ) / fator
      }

      const {
          numero,
          separadorMilhar,
          separadorDecimal,
          colocarMoeda,
          colocarNegativo
      } = params
  
      let qtdCasasMilhares = 3
      let stringBuffer = []
      let parteDecimal = arredondar(Math.abs(numero)%1)
      let parteInteira = Math.trunc(numero)
      let parteInteiraString = Math.abs(parteInteira).toString()
      let parteInteiraTamanho = parteInteiraString.length
  
      let c = 1
      while (parteInteiraString.length > 0) {
          if (c % qtdCasasMilhares == 0) {
              stringBuffer.push( `${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c )}`)
              parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c )
          } else if (parteInteiraString.length < qtdCasasMilhares) {
              stringBuffer.push( parteInteiraString )
              parteInteiraString = ''
          } 
          c++
      }
      stringBuffer.push( parteInteiraString )
      
      let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0')
      const numeroFormatado = `${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decimalString }`;
      return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
  }

  //Tudo que é público
  return {
      imprimirBRL: (numero) => 
          imprimirMoeda({
              numero,
              separadorMilhar : '.',
              separadorDecimal: ',',
              colocarMoeda : numeroFormatado => `R$ ${ numeroFormatado }`,
              colocarNegativo : numeroFormatado => `-${ numeroFormatado }`,
          }),
          imprimirGBP: (numero)=>
              imprimirMoeda({
                  numero,
                  separadorMilhar : ',',
                  separadorDecimal: '.',
                  colocarMoeda : numeroFormatado => `£ ${ numeroFormatado }`,
                  colocarNegativo : numeroFormatado => `-${ numeroFormatado }`,
              }),
          imprimirFR: (numero) =>
              imprimirMoeda({
                  numero,
                  separadorMilhar : '.',
                  separadorDecimal: ',',
                  colocarMoeda : numeroFormatado => `${ numeroFormatado } €`,
                  colocarNegativo : numeroFormatado => `-${ numeroFormatado }`,
              })
      }

})()

// console.log(moedas.imprimirBRL(0));
// console.log(moedas.imprimirBRL(3498.99));
// console.log(moedas.imprimirBRL(-3498.99));
// console.log(moedas.imprimirBRL(2313477.0135));

// console.log(moedas.imprimirGBP(0));
// console.log(moedas.imprimirBRL(3498.99));
// console.log(moedas.imprimirBRL(-3498.99));
// console.log(moedas.imprimirBRL(2313477.0135));

function cardapioIFood( veggie = true, comLactose = false ) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  // i = cardapio.length

  if ( !comLactose ) {
    cardapio.push('pastel de queijo')
  }
  cardapio = [...cardapio,'pastel de carne','empada de legumes marabijosa' ]
  // cardapio = cardapio.concat( [
  //   'pastel de carne',
  //   'empada de legumes marabijosa'
  // ] )

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)

    cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1)
    cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
  }

  let resultado = cardapio
                        //.filter(alimento => alimento == 'cuca de uva')
                        .map(alimento => alimento.toUpperCase());
  return resultado;

}
console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

function criarSanduiche(pao, recheio, queijo) {
    console.log(`Seu sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)
}

const ingredientes = ['3 queijos', 'Frango', 'cheddar']
criarSanduiche(... ingredientes);

function receberValoresIndefinidos(...valores){
  valores.map(valor => console.log(valor));
}
receberValoresIndefinidos([1, 3, 4, 5]);

console.log(... "Juliana");