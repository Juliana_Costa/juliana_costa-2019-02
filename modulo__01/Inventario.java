import java.util.*;

public class Inventario{
    private ArrayList<item> items;
    
    public Inventario(int quantidade){
        this.items = new ArrayList<>(quantidade);
    }
    
    public item obter(){
        return null;
    }
    
    public ArrayList<item> getItens(){
        return this.items;
    }
    
    public item obter(int posicao){
        if (posicao >= this.items.size()){
            return null;
        }
        return this.items.get(posicao);
    }
    
    public void remover(item item){
        this.items.remove(item);
    }
    
    public void adicionar(item item){
        this.items.add(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0; i < items.size(); i++){
            item item = this.items.get(i);
            if(items != null){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        
        return descricoes.length() > 0 ? descricoes.substring(0,(descricoes.length()-1)) : descricoes.toString();
    }
    
    public item getItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        for(int i = 0; i < this.items.size(); i++){
            item item = this.items.get(i);
            if(items != null){
                if(item.getQuantidade() > maiorQuantidade){
                    maiorQuantidade = item.getQuantidade();
                    indice = i;
                }
            }
        }
        
        return this.items.size() > 0 ? this.items.get(indice) : null;
    }
    
    public item Buscar(String descricao){
        for ( item itemAtual : this.items){
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            if (encontrei){
                return itemAtual;
            }
        }
        return null;
    }
   
    
    public boolean verificaEscudo(){
        for ( item itemAtual : this.items){
            boolean encontrei = itemAtual.getDescricao().equals("Escudo");
            if (encontrei){
                return true;
            }
        }
        return false;
    }
    
    public ArrayList<item> inverter(){
        ArrayList<item> listaInvertida = new ArrayList<>(this.items.size());
        
        for ( int i = this.items.size() -1; i>= 0; i--){
            listaInvertida.add(this.items.get(i));
        }
        
        return listaInvertida;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
        
    public void ordenarItens(TipoOrdenacao ordenacao){
        for (int i = 0; i < this.items.size(); i++){
            for (int j = 0; j < this.items.size() -1; j++){
                item atual = this.items.get(j);
                item proximo = this.items.get(j);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
                if(deveTrocar){
                    item itemTrocado = atual;
                    this.items.set(j, proximo);
                    this.items.set(j + 1, itemTrocado);
                }
            }
        }
    }
}