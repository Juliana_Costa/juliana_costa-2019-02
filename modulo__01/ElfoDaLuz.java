import java.util.*;

public class ElfoDaLuz extends Elfo{
    private int qtdAtaques;
    private final double QTD_VIDA_GANHA = 10;
    
    {
        qtdAtaques=0;
    }
    
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
        "Espada de Galvorn" 
        
        )
    );
    
    public ElfoDaLuz(String nome){
        super(nome);
        qtdDano=21;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_VALIDAS.get(0)));
        //this.inventario.adicionar(new item(1,"Espada de Galvorn"));
        //this.qtdExperienciaPorAtaque = 1;
    }
    
    private boolean devePerdeVida(){
        
        return qtdAtaques %2==1;
    }
    
    public void ganharVida(){
        
        vida+= QTD_VIDA_GANHA;
    }
    
    public item getEspada(){
        
        return this.getInventario().Buscar(DESCRICOES_VALIDAS.get(0));
    }
    public void perderItem(item item){
        
        boolean possoPerder= !DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }
    
    public void atacarComEspada(Dwarf dwarf){
        if(getEspada().getQuantidade()>0){
            qtdAtaques++;
            dwarf.diminuirVida();
            this.aumentarXp();
            if(devePerdeVida()){
            diminuirVida();
            }
            else{
            ganharVida();
            }
        
        }
    }
}