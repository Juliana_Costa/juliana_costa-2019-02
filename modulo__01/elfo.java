public class Elfo extends Personagem{
    private int indiceFlecha;
    private static int qtdElfos;

    {
        indiceFlecha = 1;
        this.inventario = new Inventario(2);
        
    }
    
    public Elfo(String nome){
        super(nome);
        this.inventario.adicionar(new item(1,"Arco"));
        this.inventario.adicionar(new item(2,"flecha"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable{//faz a limpeza de metodos que estao ociosos na memoria
        Elfo.qtdElfos--;//quando ele parar de ser usado quero que ele pare de contar da minha lista esse qtdElfos
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
   
    public boolean podeAtirarFlecha(){
        return getFlecha().getQuantidade() > 0; //so pode atirar se a quantidade de flechas for maior que 0
    }
    
    public void atirarFlechaDwarf(Dwarf dwarf){
        int qtdAtual = getFlecha().getQuantidade();// vou colocar em qtdAtual a quantidade de flechas existentes
        if (podeAtirarFlecha()){//se a quantidade for maior que 0 posso atirar
            this.getFlecha().setQuantidade(qtdAtual - 1);
            this.aumentarXp();
            dwarf.diminuirVida();
            diminuirVida();
        }
    }
    
    public String imprimirResultado(){
        return "Elfo";
    }
}
