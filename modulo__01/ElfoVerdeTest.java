

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest{
    
    @Test
    public void verificaSeElfoNasceComArcoEFlecha(){
        ElfoVerde novoElfoVerde = new ElfoVerde("verdão");
        assertEquals(2,novoElfoVerde.getQtdFlecha());
        assertEquals("Arco,Flecha",novoElfoVerde.getInventario().getDescricoesItens());
    }
    
    @Test
    public void elfoVerdeGnha2XPPorUmaFlecha(){
        ElfoVerde celebron= new ElfoVerde("Celebron");
        celebron.atirarFlechaDwarf(new Dwarf("Balin"));
        assertEquals(2, celebron.getExperiencia());
    
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida(){
        ElfoVerde celebron= new ElfoVerde("Celebron");
        item ArcoDeVidro = new item (1, "Arco de Vidro");
        celebron.ganharItem(ArcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new item(1, "Arco"), inventario.obter(0));
        assertEquals(new item(2, "Flecha"), inventario.obter(1));
        assertEquals(ArcoDeVidro, inventario.obter(2));
    
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida(){
        ElfoVerde celebron= new ElfoVerde("Celebron");
        item arcoDeMadeira = new item (1, "Arco de Madeira");
        celebron.ganharItem(arcoDeMadeira);
        Inventario inventario = celebron.getInventario();
        assertEquals(new item(1, "Arco"), inventario.obter(0));
        assertEquals(new item(2, "Flecha"), inventario.obter(1));
        assertNull(inventario.Buscar("Arco de Madeira"));
    
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoValida(){
        ElfoVerde celebron= new ElfoVerde("Celebron");
        item arcoDeVidro = new item (1, "Arco de vidro");
        celebron.ganharItem(arcoDeVidro);
        celebron.perderItem(arcoDeVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new item(1, "Arco"), inventario.obter(0));
        assertEquals(new item(2, "Flecha"), inventario.obter(1));
        assertNull(celebron.inventario.obter(2));
    
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoInvalida(){
        ElfoVerde celebron= new ElfoVerde("Celebron");
        item arco = new item (1, "Arco");
        celebron.perderItem(arco);
        Inventario inventario = celebron.getInventario();
        assertEquals(new item(1, "Arco"), inventario.obter(0));
        assertEquals(new item(2, "Flecha"), inventario.obter(1));
        
    }
    
}
