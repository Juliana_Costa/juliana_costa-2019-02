

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    private final double DELTA = 1e-9;
    
    @Test
    public void ElfoDaLuzAtacaComEspada1VezPerde21deVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("feanor ");
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(79.0,feanor.getVida(), DELTA);
    }
    
    @Test
    public void ElfoDaLuzAtacaComEspada2VezPerde21deVidaMasGanha10(){
        ElfoDaLuz feanor = new ElfoDaLuz("feanor ");
        feanor.atacarComEspada(new Dwarf("Farlum"));
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(89.0,feanor.getVida(), DELTA);
    }
    
    @Test
    public void elfoDaLuzDevePerderVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("feanor ");
        Dwarf gul= new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        assertEquals(89.0,feanor.getVida(), DELTA);
        assertEquals(100,gul.getVida(), DELTA);
    }
    
    @Test
    public void elfoDaLuzDevePerderVidaEGanhar(){
        ElfoDaLuz feanor = new ElfoDaLuz("feanor ");
        Dwarf gul= new Dwarf("Gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89.0,feanor.getVida(), DELTA);
        assertEquals(90.0,gul.getVida(), DELTA);
    }
    
}
