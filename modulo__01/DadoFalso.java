

public class DadoFalso implements Sorteador
{
    private int valorFalso;
    //to forçando ele a ter o valor que eu quero
    public void simularValor(int valor){
       this.valorFalso = valor;
    }
    
    public int sortear(){
        return this.valorFalso;
    }
}
