

public class DwarfBarbaLonga extends Dwarf 
{
    //criar teste do DwarfBarbaLonga e usar aqueles dados pra validar o teste
    private Sorteador sorteador;
    
    public DwarfBarbaLonga(String nome){
        super(nome);
        sorteador = new DadoD6();
    }
    
    public DwarfBarbaLonga(String nome, Sorteador sorteador){
        this(nome);
        this.sorteador = sorteador;
    }
    
    public void diminuirVida(){
        boolean devePerderVida = sorteador.sortear()<=4;
        if(devePerderVida){
            super.diminuirVida();
        }
    }
}
