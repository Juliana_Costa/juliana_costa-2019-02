import java.util.*;

public class Exercito extends Elfo {
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(Arrays.asList(ElfoVerde.class,ElfoNoturno.class));
    //HashMap<String, Elfo> exercitoElfos = new HashMap<>();
    //HashMap é sempre chave e valor, chave valor que ele busca pra trazer o valor
    private ArrayList<Elfo>elfos = new ArrayList<>(); // fez um arraylist de elfo  de toodos os status
    private HashMap<Status, ArrayList<Elfo>>porStatus = new HashMap<>();
    
    public Exercito(String nome){
        super(nome);
    }
    
    // esta definindo por status pra jogar no arrayList uma lista de Elfos
    public void alistar(Elfo elfo){ // quando tu colocando um elfo novo
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            elfos.add(elfo);//aqui ele ta adicionando o elfo no arraylist
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());//aqui ele ta verificando o status e colocando ele em cada linha do HashMap
            if(elfoDoStatus==null){// se for nulo ele ta iniciando um novo arrayList 
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(),elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo>getElfos(){
        return this.elfos;
        
    }
    
    public ArrayList<Elfo> Buscar(Status status){
        return this.porStatus.get(status);
    }
}
