
public class item
{ 
    protected int quantidade;
    private String descricao;
    
    public item (int quantidade, String descricao){
        this.quantidade = quantidade;
        this.descricao = descricao;
    }
    
    public int getQuantidade(){
        return this.quantidade;
    }
    
    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;
    }
    
    public void acrescentaMaisUm(){
        quantidade++;
    }
    
    public String getDescricao(){
        return this.descricao;
    }
    
    public void setDescricao(String descricao){
        this.descricao = descricao;
    }
    
    public boolean equals(Object obj){
        item outroItem = (item)obj;
        return this.quantidade == outroItem.getQuantidade() && this.descricao.equals(outroItem.getDescricao());
    }
}
