

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AgendaContatosTest
{
    @Test
    public void adicionarContatoEPesquisar(){
      AgendaContatos agenda= new AgendaContatos();
      agenda.adicionar("Juliana","123123123");
      agenda.adicionar("DBC", "55555");
      assertEquals("123123123", agenda.consultar("Juliana"));
        
    }
    
    @Test
    public void adicionarContatoEPesquisarPorTelefone(){
      AgendaContatos agenda= new AgendaContatos();
      agenda.adicionar("Juliana","123123123");
      agenda.adicionar("DBC", "55555");
      assertEquals("DBC", agenda.consultarNome("55555"));
        
    }
    
    @Test
    public void adicionarContatoEGerarCSV(){
      AgendaContatos agenda= new AgendaContatos();
      agenda.adicionar("Juliana","123123123");
      agenda.adicionar("DBC", "55555");
      String separador = System.lineSeparator();
      String esperado = String.format("Juliana,123123123%sDBC,55555%s", separador,separador);
      assertEquals(esperado, agenda.csv());
        
    }
}
