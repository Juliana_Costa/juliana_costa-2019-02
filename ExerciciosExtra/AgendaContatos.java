import java.util.*;

public class AgendaContatos
{
    private HashMap<String, String>contatos;
    
    public AgendaContatos(){
        contatos = new LinkedHashMap<>();//traz ordenado
    }
    
    public void adicionar(String nome, String telefone){
        contatos.put(nome,telefone);
    }
    
    public String consultar(String nome){
        return contatos.get(nome);
    }
    
    public String consultarNome(String telefone){
        for(HashMap.Entry<String, String>par :contatos.entrySet()){// par é o hashmap de uma linha, ou seja em cada passada ele
            if(par.getValue().equals(telefone)){//pega uma chave e valor
                return par.getKey();
            }
        }
        return null;// se for eu devolvo a chave se não for eu devolvo nulo
    }
    
    public String csv(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();// tipo o \n
        for(HashMap.Entry<String, String>par :contatos.entrySet()){// par é o hashmap de uma linha, ou seja em cada passada ele
            String chave = par.getKey();// aqui ele ta atribuindo aqueles valores a essas variaveis pra depois formatar do jeito solicitado
            String valor = par.getValue();
            String contato = String.format("%s,%s%s",chave,valor,separador);
            builder.append(contato);
        }
        return builder.toString();
    }
}
